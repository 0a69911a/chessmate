#ifndef CHESSBOARD_H
#define CHESSBOARD_H

#include "chessfigure.h"

#include <QFrame>
#include <QLabel>
#include <QMouseEvent>
#include <QPair>

class ChessBoard : public QFrame
{
    Q_OBJECT

public:
    explicit ChessBoard(QWidget *parent = nullptr);
    QLabel *label;
    QPair<int, int> size = QPair<int, int>(8, 8);
    QList<ChessFigure *> alive;
    QList<ChessFigure *> killed;
    QList<QPair<QPair<int, int>, bool>> possible_turn_positions;
    QString team1 = "white";
    QString team2 = "black";
    bool is_started = false;
    bool turn_in_progress = false;
    bool team_turn = true;
    ChessFigure *turn_figure;
    int field_size = 45;

    void fill_board();
    void clear_board();
    void set_size(int x, int y);
    bool move_turn_figure(QPair<int, int> position);
    QPair<int, int> find_position(int x, int y);

    ~ChessBoard();
signals:

public slots:
    void start();

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void draw_board(QPainter &painter);
    void draw_square(QPainter &painter, int x, int y, int color);
    void draw_transparent_image(QPainter &painter, QPair<QPair<int, int>, bool> position);
    void set_possible_moves_and_beats(ChessFigure *figure);
    void print_winner(bool team);
};

#endif // CHESSBOARD_H
