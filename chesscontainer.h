#ifndef CHESSCONTAINER_H
#define CHESSCONTAINER_H

#include "chessboard.h"

#include <QGridLayout>
#include <QWidget>

class ChessContainer : public QWidget
{
    Q_OBJECT
public:
    explicit ChessContainer(QWidget *parent = nullptr);
    ~ChessContainer();

    ChessBoard *chessboard;
    QGridLayout *team1_lost;
    QGridLayout *team2_lost;
signals:

public slots:

};

#endif // CHESSCONTAINER_H
