#ifndef CHESSWINDOW_H
#define CHESSWINDOW_H

#include "chessboard.h"

#include <QWidget>
#include <qgridlayout.h>

namespace Ui {
class ChessWindow;
}

class ChessWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ChessWindow(QWidget *parent = 0);
    ~ChessWindow();

    ChessBoard *chessboard;
    QGridLayout *team1_lost;
    QGridLayout *team2_lost;
    QGridLayout *gameLayout;

    QLabel *label;

private slots:

private:
    Ui::ChessWindow *ui;
protected:
    void print_text(const QString string);
};

#endif // CHESSWINDOW_H
