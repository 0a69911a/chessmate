#include <iostream>

#include "chessboard.h"
#include "chessfigure.cpp"

#include <QMouseEvent>
#include <QRgb>
#include <QPair>
#include <QLabel>
#include <QMessageBox>

#define white true
#define black false

ChessBoard::ChessBoard(QWidget *parent) : QFrame(parent){
    setFrameStyle(QFrame::Panel | QFrame::Sunken);
    setFocusPolicy(Qt::StrongFocus);

    is_started = false;
    this->update();
}

ChessBoard::~ChessBoard(){

}

void ChessBoard::fill_board(){
    std::cout << "fill board" << std::endl;
    alive.clear();
    killed.clear();
    for (int color = 0; color < 2; ++color){
        for (int y = 0; y < 2; ++y){
            for (int x = 0; x < 8; ++x){
                if (y == 1){
                    PawnFigure *pawn = new PawnFigure(x, color ? y : 7 - y, color ? white : black);
                    this->alive.append(pawn);
                } else {
                    if (x == 0 || x == 7){
                        RookFigure *rook = new RookFigure(x, color ? y : 7 - y, color ? white : black);
                        this->alive.append(rook);
                    } else if (x == 1 || x == 6){
                        HorseFigure *horse = new HorseFigure(x, color ? y : 7 - y, color ? white : black);
                        this->alive.append(horse);
                    } else if (x == 2 || x == 5) {
                        OfficerFigure *officer = new OfficerFigure(x, color ? y : 7 - y, color ? white : black);
                        this->alive.append(officer);
                    } else if (x == 3){
                        if (color == 0){
                            KingFigure *figure = new KingFigure(x, color ? y : 7 - y, color ? white : black);
                            this->alive.append(figure);
                        } else {
                            QueenFigure *figure = new QueenFigure(x, color ? y : 7 - y, color ? white : black);
                            this->alive.append(figure);
                        }
                    } else if (x == 4){
                        if (color == 1){
                            KingFigure *figure = new KingFigure(x, color ? y : 7 - y, color ? white : black);
                            this->alive.append(figure);
                        } else {
                            QueenFigure *figure = new QueenFigure(x, color ? y : 7 - y, color ? white : black);
                            this->alive.append(figure);
                        }
                    }
                }
            }
        }
    }
}

void ChessBoard::clear_board(){
    QPainter painter(this);
    for (int i = 0; i < size.first; ++i){
        for (int j = 0; j < size.second; ++j){
            draw_square(painter, i, j, (i + j) % 2);
        }
    }
}

void ChessBoard::start(){
    is_started = true;
    this->fill_board();
    this->update();
}

void ChessBoard::set_size(int x, int y){
    this->size = QPair<int, int>(x, y);
}

void ChessBoard::draw_board(QPainter &painter){
    for (int i = 0; i < size.first; ++i){
        for (int j = 0; j < size.second; ++j){
            draw_square(painter, i, j, (i + j) % 2);
        }
    }
}

void ChessBoard::paintEvent(QPaintEvent *event){
    std::cout << "paint event called" << std::endl;
    QFrame::paintEvent(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    QRect rect = contentsRect();

    draw_board(painter);

    if(is_started){
        for (int i = 0; i < alive.size(); ++i){
            alive[i]->draw(painter, rect.topLeft(), field_size);
        }

        if (turn_in_progress){
            for (int i = 0; i < possible_turn_positions.size(); ++i){
                draw_transparent_image(painter, possible_turn_positions[i]);
            }
        }
        for (int i = 0; i < killed.size(); ++i){
            killed[i]->draw(painter, QPoint(0, 400), field_size / 2);
        }
    }
}

void ChessBoard::draw_square(QPainter &painter, int x, int y, int int_color){
    QColor color = int_color ? QColor("white") : QColor("red");
    painter.fillRect(x * field_size, y * field_size, field_size, field_size, color);
}

void ChessBoard::draw_transparent_image(QPainter &painter, QPair<QPair<int, int>, bool> position){
    painter.fillRect(position.first.first * field_size, position.first.second * field_size,
                     field_size, field_size, QBrush(position.second ? QColor(255, 162, 0, 128) : QColor(128, 128, 255, 128)));
}

void ChessBoard::mousePressEvent(QMouseEvent *event){
    std::cout << "mouse press event" << std::endl;
    qDebug() << event->x() << event->y() << "eveent";
    if (is_started && (0 <= event->x() && event->x() <= 8 * field_size) && (0 <= event->y() && event->y() <= 8 * field_size)){
        QPair<int, int> pos = find_position(event->x(), event->y());
        qDebug() << pos.first << pos.second << "position";
        if (turn_in_progress){
            bool break_ = false;
            for (int i = 0; i < possible_turn_positions.size(); ++i){
                if (pos.first == possible_turn_positions[i].first.first && pos.second == possible_turn_positions[i].first.second){
                    bool turn_completed = move_turn_figure(pos);
                    if (turn_completed){
                        turn_in_progress = false;
                        possible_turn_positions = QList<QPair<QPair<int, int>, bool>>();
                        team_turn = !team_turn;
                        break_ = true;
                        break;
                    } else return;
                }
            }
            if (!break_){
                turn_in_progress = false;
                possible_turn_positions = QList<QPair<QPair<int, int>, bool>>();
            }
        } else {
            for (int i = 0; i < alive.size(); ++i){
                if (pos.first == alive[i]->x && pos.second == alive[i]->y && team_turn == alive[i]->team){
                    turn_in_progress = true;
                    turn_figure = alive[i];
                    set_possible_moves_and_beats(alive[i]);
                    if (possible_turn_positions.isEmpty()){
                        turn_in_progress = false;
                        turn_figure = nullptr;
                        break;
                    }
                    break;
                }
            }
        }
    }
    this->update();
    return;
}

QPair<int, int> ChessBoard::find_position(int x, int y){
    return QPair<int, int>(x / field_size, y / field_size);
}

bool ChessBoard::move_turn_figure(QPair<int, int> position){
    for (int i = 0; i < alive.size(); ++i){
        if (alive[i]->x == position.first && alive[i]->y == position.second){
            if (alive[i]->team == turn_figure->team){
                return false;
            }
            alive[i]->x = killed.size();
            alive[i]->y = alive[i]->team;
            killed.append(alive[i]);
            if (alive[i]->is_target){
                is_started = false;
                qDebug() << "print winner";
                print_winner(turn_figure->team);
            }
            alive.removeAt(i);
            break;
        }
    }
    this->turn_figure->x = position.first;
    this->turn_figure->y = position.second;
    return true;
}

void ChessBoard::set_possible_moves_and_beats(ChessFigure *figure){
    qDebug() << "set possible moves";
    QList<QList<QPair<int, int>>> moves = figure->possible_moves();
    for (int i = 0; i < moves.size(); ++i){
        for (int j = 0; j < moves[i].size(); ++j){
            bool break_ = false;
            for (int alive_index = 0; alive_index < alive.size(); ++alive_index){
                if (moves[i][j] == QPair<int, int>(alive[alive_index]->x, alive[alive_index]->y)) {
                    qDebug() << turn_figure->team << turn_figure->x << turn_figure->y << turn_figure->beat_is_move << "string";
                    if (alive[alive_index]->team != turn_figure->team && figure->beat_is_move){
                        qDebug() << "here i am" << alive[alive_index]->x << alive[alive_index]->y;
                        possible_turn_positions.append(QPair<QPair<int, int>, bool>(moves[i][j], true));
                    }
                    break_ = true;
                    break;
                }
            }
            if (!break_){
                possible_turn_positions.append(QPair<QPair<int, int>, bool>(moves[i][j], false));
            } else break;
        }
    }
    if (!figure->beat_is_move){
        QList<QList<QPair<int, int>>> beats = figure->possible_beats();
        for (int i = 0; i < beats.size(); ++i){
            for (int j = 0; j < beats[i].size(); ++j){
                for (int alive_index = 0; alive_index < alive.size(); ++alive_index){
                    if (beats[i][j] == QPair<int, int>(alive[alive_index]->x, alive[alive_index]->y)) {
                        if (alive[alive_index]->team != turn_figure->team){
                            possible_turn_positions.append(QPair<QPair<int, int>, bool>(beats[i][j], true));
                        }
                        break;
                    }
                }
            }
        }
    }
}

void ChessBoard::print_winner(bool team){
    QMessageBox message_box(QMessageBox::Information, QString("end."), QString("The winner is ") + QString(team ? team1 : team2), QMessageBox::Ok);
    message_box.exec();
}
