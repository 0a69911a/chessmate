#include "chessfigure.h"

#include <QDebug>

bool ChessFigure::could_beat(int x, int y){
    return this->could_move(x, y);
}

bool ChessFigure::could_move(int x, int y){}

void ChessFigure::draw(QPainter &painter, QPoint left_top, int field_size){}

QList<QList<QPair<int, int>>> ChessFigure::possible_moves(){}

QList<QList<QPair<int, int>>> ChessFigure::possible_beats(){ return QList<QList<QPair<int, int>>>(); }

class PawnFigure: public ChessFigure{
    bool could_move_y(int y){
        return ((this->team && this->y - 1 == y) || (!this->team && this->y + 1 == y));
    }
public:
    PawnFigure(int x = 0, int y = 0, bool team = false){
        this->x = x;
        this->y = y;
        this->team = team;
        this->beat_is_move = false;
    }
    bool could_move(int x, int y){
        return (this->could_move_y(y) && (this->x == x));
    }
    bool could_beat(int x, int y){
        return (this->could_move_y(y) && (this->x + 1 == x || this->x - 1 == x));
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_pawn.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }

    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        QList<QPair<int, int>> point1;
        QList<QPair<int, int>> point2;
        if (this->team && y < 7){
            point1.append(QPair<int, int>(this->x, this->y + 1));
            if (y == 1) point1.append(QPair<int, int>(this->x, this->y + 2));
            results.append(point1);
        } else if (y > 0){
            point2.append(QPair<int, int>(this->x, this->y - 1));
            if (y == 6) point2.append(QPair<int, int>(this->x, this->y - 2));
            results.append(point2);
        }
        return results;
    }
    QList<QList<QPair<int, int>>> possible_beats(){
        QList<QList<QPair<int, int>>> results;
        QList<QPair<int, int>> point1;
        QList<QPair<int, int>> point2;
        if (this->team && y < 7){
            if (x > 0) point1.append(QPair<int, int>(this->x - 1, this->y + 1));
            if (x < 7) point2.append(QPair<int, int>(this->x + 1, this->y + 1));
            results.append(point1);
            results.append(point2);
        } else if (y > 0){
            if (x > 0) point1.append(QPair<int, int>(this->x - 1, this->y - 1));
            if (x < 7) point2.append(QPair<int, int>(this->x + 1, this->y - 1));
            results.append(point1);
            results.append(point2);
        }
        return results;
    }
};

class HorseFigure: public ChessFigure{
public:
    using ChessFigure::ChessFigure;
    bool could_move(int x, int y){
        return (((this->x + 2 == x || this->x - 2 == x) && (this->y + 1 == y || this->y - 1 == y)) ||
                ((this->y + 2 == y || this->y - 2 == y) && (this->x + 1 == x || this->x - 1 == x)));
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_horse.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }

    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        QList<QPair<int, int>> point1;
        QList<QPair<int, int>> point2;
        QList<QPair<int, int>> point3;
        QList<QPair<int, int>> point4;
        QList<QPair<int, int>> point5;
        QList<QPair<int, int>> point6;
        QList<QPair<int, int>> point7;
        QList<QPair<int, int>> point8;
        if (y < 7 && x < 6){
            point1.append(QPair<int, int>(this->x + 2, this->y + 1));
            results.append(point1);
        }
        if (y < 6 && x < 7){
            point2.append(QPair<int, int>(this->x + 1, this->y + 2));
            results.append(point2);
        }
        if (y > 1 && x < 7){
            point3.append(QPair<int, int>(this->x + 1, this->y - 2));
            results.append(point3);
        }
        if (y > 0 && x < 6){
            point4.append(QPair<int, int>(this->x + 2, this->y - 1));
            results.append(point4);
        }
        if (y < 7 && x > 1){
            point5.append(QPair<int, int>(this->x - 2, this->y + 1));
            results.append(point5);
        }
        if (y < 6 && x > 0){
            point6.append(QPair<int, int>(this->x - 1, this->y + 2));
            results.append(point6);
        }
        if (y > 1 && x > 0){
            point7.append(QPair<int, int>(this->x - 1, this->y - 2));
            results.append(point7);
        }
        if (y > 0 && x > 1){
            point8.append(QPair<int, int>(this->x - 2, this->y - 1));
            results.append(point8);
        }
        return results;
    }
};

class OfficerFigure: public ChessFigure{
public:
    using ChessFigure::ChessFigure;
    bool could_move(int x, int y){
        return ((this->x - this->y == x - y) || (this->x + this->y == x + y));
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_bishop.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }

    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        QList<QPair<int, int>> point1;
        QList<QPair<int, int>> point2;
        QList<QPair<int, int>> point3;
        QList<QPair<int, int>> point4;
        for (int i = 1; i <= x; ++i){
            qDebug() << i;
            if (y - i >= 0){
                point1.append(QPair<int, int>(x - i, y - i));
            }
            if (y + i < 8){
                point2.append(QPair<int, int>(x - i, y + i));
            }
        }
        for (int i = 1; i < 8 - x; ++i){
            if (y - i >= 0){
                point3.append(QPair<int, int>(x + i, y - i));
            }
            if (y + i < 8){
                point4.append(QPair<int, int>(x + i, y + i));
            }
        }
        results.append(point1);
        results.append(point2);
        results.append(point3);
        results.append(point4);
        return results;
    }
};

class RookFigure: public ChessFigure{
public:
    using ChessFigure::ChessFigure;
    bool could_move(int x, int y){
        return (this->x == x || this->y == y);
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_rook.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }
    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        QList<QPair<int, int>> point1;
        QList<QPair<int, int>> point2;
        QList<QPair<int, int>> point3;
        QList<QPair<int, int>> point4;
        for (int i = x - 1; i >= 0; --i) point1.append(QPair<int, int>(i, y));
        for (int i = x + 1; i < 8; ++i) point2.append(QPair<int, int>(i, y));
        for (int j = y - 1; j >= 0; --j) point3.append(QPair<int, int>(x, j));
        for (int j = y + 1; j < 8; ++j) point4.append(QPair<int, int>(x, j));
        results.append(point1);
        results.append(point2);
        results.append(point3);
        results.append(point4);
        return results;
    }
};

class QueenFigure: public ChessFigure{
public:
    using ChessFigure::ChessFigure;
    bool could_move(int x, int y){
        return this->x == x || this->y == y || (this->y + this->x == x + y) || (this->y - this->x == y - x);
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_queen.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }
    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        RookFigure rook(x, y, team);
        QList<QList<QPair<int, int>>> results_rook = rook.possible_moves();
        OfficerFigure officer(x, y, team);
        QList<QList<QPair<int, int>>> results_officer = officer.possible_moves();
        results.append(results_rook);
        results.append(results_officer);
        return results;
    }
};

class KingFigure: public ChessFigure{
public:
    KingFigure(int x = 0, int y = 0, bool team = false){
        this->x = x;
        this->y = y;
        this->team = team;
        this->is_target = true;
    }
    bool could_move(int x, int y){
        return abs(this->x - x) <= 1 && abs(this->y - y) <= 1;
    }
    void draw(QPainter &painter, QPoint left_top, int field_size){
        QString static_url;
        static_url = static_path + QString(team ? "white" : "black") + "_king.png";
        QPixmap pixmap(static_url);
        painter.drawPixmap(left_top.x() + this->x * field_size, left_top.y() + this->y * field_size, field_size, field_size, pixmap);
    }
    QList<QList<QPair<int, int>>> possible_moves(){
        QList<QList<QPair<int, int>>> results;
        qDebug() << x << y;
        for (int i = x - 1; i <= x + 1; ++i){
            for (int j = y - 1; j <= y + 1; ++j){
                if (i < 8 && i >= 0 && j < 8 && j >= 0 && !(x == i && y == j)){
                    QList<QPair<int, int>> list;
                    list.append(QPair<int, int>(i, j));
                    results.append(list);
                }
            }
        }
        return results;
    }
};

