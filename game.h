#ifndef GAME_H
#define GAME_H

#include "chessboard.h"
#include "chesscontainer.h"

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
class Game;
}

class Game : public QMainWindow
{
    Q_OBJECT

public:
    explicit Game(QWidget *parent = 0);
    ~Game();

private:
    Ui::Game *ui;
    ChessBoard *board;
    ChessContainer *container;
    QPushButton *start;
};

#endif // GAME_H
