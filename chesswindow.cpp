#include "chesswindow.h"
#include "ui_chesswindow.h"

ChessWindow::ChessWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChessWindow)
{
    ui->setupUi(this);
    chessboard = new ChessBoard(this);
    connect(ui->NewGame, &QPushButton::clicked, chessboard, &ChessBoard::start);
    gameLayout = new QGridLayout;
    label = new QLabel("text");
    gameLayout->addWidget(label);
    setLayout(gameLayout);
}

ChessWindow::~ChessWindow()
{
    delete ui;
}

void ChessWindow::print_text(const QString string){
    this->label->setText(string);
    this->update();
}
