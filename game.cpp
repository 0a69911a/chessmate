#include "game.h"
#include "ui_game.h"

#include <QHBoxLayout>
#include <QLabel>

Game::Game(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Game)
{
    ui->setupUi(this);
    this->statusBar();

    QGridLayout *layout = new QGridLayout;

    this->board = new ChessBoard();
    board->set_size(8, 8);
    board->fill_board();
    board->setFixedWidth(360);
    board->setFixedHeight(460);

    start = new QPushButton(QString("Start game"));
    start->setFocusPolicy(Qt::NoFocus);
    start->setFixedHeight(180);
    start->setFixedWidth(180);

    connect(start, &QPushButton::clicked, board, &ChessBoard::start);

//    this->container = new ChessContainer(this->board);

//    layout->addWidget(this->container);
    layout->addWidget(start, 0, 0);
    layout->addWidget(this->board, 0, 1, 1, 1);
//    setLayout(layout);

    QWidget *content = new QWidget();
    content->setLayout(layout);
    setCentralWidget(content);
}

Game::~Game()
{
    delete ui;
}
