#ifndef CHESSFIGURE_H
#define CHESSFIGURE_H

#include <QPainter>
#include <QString>
#include <QWidget>

class ChessFigure: public QObject
{
public:
    ChessFigure(int x = 0, int y = 0, bool team = false){
        this->x = x;
        this->y = y;
        this->team = team;
    }
    ~ChessFigure(){}
    virtual bool could_move(int x, int y);
    virtual bool could_beat(int x, int y);
    virtual void draw(QPainter &painter, QPoint left_corner, int field_size);
    virtual QList<QList<QPair<int, int>>> possible_moves();
    virtual QList<QList<QPair<int, int>>> possible_beats();
    bool team;
    bool is_target = false;
    int x, y;
    bool beat_is_move = true;
protected:
    const QString static_path = QString(":/static/static/");
public:
};

#endif // CHESSFIGURE_H
