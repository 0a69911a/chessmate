#include "game.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Game w;
    w.setFixedHeight(460);
    w.setFixedWidth(680);
    w.show();

    return a.exec();
}
